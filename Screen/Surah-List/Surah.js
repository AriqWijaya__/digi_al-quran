import React, {useEffect, useState} from 'react'
import { StyleSheet, Text, View , Button, SafeAreaView, TouchableOpacity} from 'react-native'
import axios from 'axios';
import { FlatList } from 'react-native-gesture-handler';
import { useSelector, useDispatch } from 'react-redux'
import { SetSurah, getSurah, setNamaSurah } from '../../CounterSlice/CounterSlice.js'

export default function Surah({ navigation }) {
    const dispatch = useDispatch()
    const [items, setItems] = useState([])
    const getData = async () => {
        try {
            const res = await axios.get('https://equran.id/api/surat')
            const data1 = (res.data)
            setItems(data1)
            console.log(items);

        } catch(error){
            console.log(error)
        }
    }


    useEffect(() => {
        getData();
    }, [])

    const gotoayat = (item) => {
        dispatch(SetSurah({value: (item.nomor)}))
        dispatch(setNamaSurah({name: (item.nama_latin), arti: (item.arti)}))
        navigation.navigate("Ayat")
    }

    return (
        
        <View style = {styles.Container}>
            <SafeAreaView>
                <FlatList 
                data = {items}
                keyExtractor = {(item) => `${item.nomor}`}
                renderItem = {({item}) => {
                    return(
                        <View style = {styles.ContentScreen}>
                            <TouchableOpacity 
                            onPress = {() => gotoayat(item)}
                            >
                            <View style = {styles.Content}>
                                <View style = {styles.NomorSurah}>
                                    <Text>{item.nomor}</Text>
                                </View>
                                <View style = {styles.SurahDetail}>
                                        <View>
                                            <Text>{item.nama_latin}</Text>
                                            <Text>{item.tempat_turun} {"|"} {item.jumlah_ayat}</Text>
                                        </View>
                                        <View>
                                            <Text style = {{fontSize: 24, fontWeight: 'bold'}}>{item.nama}</Text>
                                        </View>
                                </View>
                            </View>
                            </TouchableOpacity>
                        </View>
                    )
                    }}/>
            </SafeAreaView>
        </View>
    )
}

const styles = StyleSheet.create({
    Container: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: '15%',
        marginBottom: '5%'
    },
    Button: {
        justifyContent: 'center',
        marginBottom: 20,
    },
    Content: {
       borderWidth: 1,
       height: 70,
       width: 300,
       marginVertical: 10,
       borderRadius: 10,
       flexDirection: 'row'
    },
   NomorSurah: {
       borderRightWidth: 1,
       paddingVertical: 10,
       width: 40,
       alignItems: 'center', 
   },
   SurahDetail: {
       paddingVertical: 10,
       paddingHorizontal: 10,
       flexDirection: 'row',
       justifyContent: 'space-between',
       width: 250,

   },
})
