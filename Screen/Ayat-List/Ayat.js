import React, {useState, useEffect} from 'react'
import { StyleSheet, Text, View, Button, TouchableOpacity, SafeAreaView, useWindowDimensions } from 'react-native'
import axios from 'axios';
import { FlatList } from 'react-native-gesture-handler';
import RenderHtml from 'react-native-render-html';
import { useFonts } from 'expo-font';
import { useSelector} from 'react-redux'
import * as Font from 'expo-font';

export default function Ayat({navigation}) {
    const [items, setItems] = useState([])
    const idSurah = useSelector((state) => state.counter.idSurah)
    const detailsurah = useSelector((state) => state.counter.detailSurah)
    
     
    const getDataayat = async () => {
        try {
            const res = await axios.get(`https://equran.id/api/surat/${idSurah}`)
            const data1 = (res.data.ayat)
            setItems(data1)
            console.log(items);

        } catch(error){
            console.log(error)
        }
    }
    useEffect(() => {
        getDataayat();
    }, [])

    const { width } = useWindowDimensions();
    const [loaded] = useFonts({
        Uthman: require('./Uthman.ttf'),
        Uthmani: require('./UthmanTN1.otf')
      });
    if (!loaded) {
        return null;
      } else {
    return (
        <View style = {styles.Container}>
            {/* <View style ={styles.Button}>
            <Button 
            title = "tekan"
            onPress = {() => getDataayat()}
            />
            </View> */}
            <View style = {styles.Header}>
            <TouchableOpacity style={styles.Button} onPress = {() => navigation.navigate("Surah")}>
                    <Text style ={styles.fontBtn}>{detailsurah.nama} | {detailsurah.arti}</Text>
                </TouchableOpacity>
            </View>

            <SafeAreaView>
                <FlatList 
                data = {items}
                keyExtractor = {(item) => `${item.nomor}`}
                renderItem = {({item}) => {
                    return(
                        <View style = {styles.ContentScreen}>
                            
                            <View style = {styles.Content}>
                                <View style = {styles.NomorSurah}>
                                    <Text>{item.nomor}</Text>
                                </View>
                                <View style = {styles.SurahDetail}>
                                        {/* <View>
                                            <Text>test</Text>
                                            <Text>{item.tempat_turun} {"|"} {item.jumlah_ayat}</Text>
                                        </View> */}
                                        
                                        <View style = {styles.ayatArab}>
                                            <Text style = {{fontSize: 30, fontFamily: 'Uthmani'}}>{item.ar}</Text>
                                        </View>
                                        <View style ={styles.ayatID}>
                                            {/* <Text>{item.tr}</Text> */}
                                            <RenderHtml
                                                contentWidth={width}
                                                source={{html: `${item.tr}`}}
                                            />
                                        </View> 
                                </View>
                            </View>
                            
                        </View>
                    )
                    }}/>
            </SafeAreaView>
        </View>
    )
}
}

const styles = StyleSheet.create({
    Container: {
        // alignItems: 'center',
        // justifyContent: 'center',
        // marginVertical: '7%' ,
        // paddingVertical:'8%'
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: '5%' ,
        flex: 1,
        paddingBottom: '10%'
    },
    Button: {
        justifyContent: 'center',
        marginBottom: 20,
    },
    Content: {
       borderWidth: 1,
       width: 300,
       marginVertical: 10,
       borderRadius: 10,
       flexDirection: 'row',
       flex: 1,
       
    },
   NomorSurah: {
       borderRightWidth: 1,
       paddingVertical: 10,
       width: 40,
       alignItems: 'center', 
   },
   SurahDetail: {
       paddingVertical: 10,
       paddingHorizontal: 10,
    //    flexDirection: 'row',
    //    justifyContent: 'flex-end',
       width: 250,
       flex: 1,
   },
   ayatArab: {
       flexDirection: 'row',
       justifyContent: 'flex-end',
      
   },
   ayatID: {
       flexDirection: 'row',
       justifyContent: "flex-start",
   
   },
   Button: {
    backgroundColor: '#2565AE', 
    alignItems: 'center', 
    paddingVertical: 10, 
    borderRadius: 60
},

fontBtn: {
    color: 'white', 
    fontWeight: '600', 
    fontSize: 16,
},
Header: {
    width: '70%',
    paddingTop: '10%',
    paddingBottom: '3%'

}
})