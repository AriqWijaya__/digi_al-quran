import React, {useState} from 'react'
import { StyleSheet, Text, View, TextInput, Button, Alert, TouchableOpacity } from 'react-native'
import { useSelector } from 'react-redux';
import * as firebase from 'firebase'

export default function Register({navigation}) {
    const [regisEmail, setRegisEmail] = useState("");
    const [regisPassw, setRegisPassw] = useState("");
    const firebaseConfig = useSelector((state) => state.counter.firebaseConfig)

    const Register= () => {
        console.log("email: ", regisEmail);
        console.log("email: ", regisPassw);
        const data = {
            regisEmail,
            regisPassw
        }  
        firebase.auth().createUserWithEmailAndPassword(regisEmail, regisPassw)
        .then(() => {
            console.log("Register: Success")
            navigation.navigate("Login")
        })
        .catch((err) => {
            console.log("Register: ", err)
            Alert.alert("Peringatan", "Register Anda Gagal, Silahkan Coba Lagi",)
        })


    }

    if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig)
    }
    return (
        <View style = {styles.Container}>
        <View style = {styles.Login}>
            <TextInput 
                style = {styles.TextInput}
                value = {regisEmail}
                onChangeText = {(value) => setRegisEmail(value)}
                placeholder = "Masukan Email Anda"
                keyboardType = "email-address"
            />

            <TextInput 
                style = {styles.TextInput}
                value = {regisPassw}
                onChangeText = {(value) => setRegisPassw(value)}
                secureTextEntry = {true}
                placeholder = "Masukan Password Anda"
            />
            <View style = {styles.Submittion}> 
                
                <TouchableOpacity style ={styles.Button}onPress = {() => navigation.navigate("Login")}>
                    <Text style ={styles.fontBtn}>Go Back</Text>
                </TouchableOpacity>

                <TouchableOpacity style ={styles.Button}onPress = {() => Register()}>
                    <Text style ={styles.fontBtn}>Register Now</Text>
                </TouchableOpacity>
                
            </View>
        </View>
    </View>
    )
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2565AE'
    },
    Submittion: {
        flexDirection: 'row',
        justifyContent: "space-between",
    },
    TextInput: {
        borderWidth: 1.3,
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 10,
    },
    Login: {
        width: '70%',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 30,
        justifyContent: 'center',
    },
    Button: {
        backgroundColor: '#2565AE', 
        alignItems: 'center', 
        borderRadius: 10,
        height: '50%',
        justifyContent: 'center',
        padding: 8

    },
    fontBtn: {
        color: 'white', 
        fontWeight: '600', 
        fontSize: 16,
    }
})
