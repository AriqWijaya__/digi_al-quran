import React from 'react'
import { View, Text } from 'react-native'
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Login from './Login/Login.js'
import Register from './Register/Register'
import Surah from './Surah-List/Surah.js'
import Home from './Home/Home.js'
import Ayat from './Ayat-List/Ayat.js'
import AboutUs from './AboutUs/AboutUs.js'
import { store } from '../Store/Store.js'
import { Provider} from 'react-redux'


const Stack = createNativeStackNavigator();
export default function Index() {

    return (
      <Provider store={store}>
       <NavigationContainer>

        <Stack.Navigator
        screenOptions ={{
          headerTitleAlign: 'center'
        }}>
          <Stack.Screen
          name = "Login"
          component ={Login}
          />

          <Stack.Screen
          name = "Register"
          component ={Register}
          />

          <Stack.Screen
          name = "Home"
          component ={Home}
          />

          <Stack.Screen 
            name='Surah' 
            component={Surah}
            options={{headerShown:false}} 
            
          />
          <Stack.Screen 
            name='Ayat' 
            component={Ayat}
            options ={{headerTintColor: "purple"}} 
          />

          <Stack.Screen 
            name='AboutUs' 
            component={AboutUs}
            options={{title: "Tentang Kami"}}
          />
        </Stack.Navigator>

      </NavigationContainer>
      </Provider>
    )
}

