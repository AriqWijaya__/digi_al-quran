import React, {useState} from 'react'
import { StyleSheet, Text, View, Button, TextInput, Alert, TouchableOpacity } from 'react-native'
import * as firebase from 'firebase'
import { useSelector } from 'react-redux';

export default function Login({navigation}) {
    const [email, setEmail] = useState("");
    const [passw, setPassw] = useState("");
    const firebaseConfig = useSelector((state) => state.counter.firebaseConfig)

    const Login = () => {
        // console.log("email: ", email);
        // console.log("email: ", passw);
        firebase.auth().signInWithEmailAndPassword(email, passw)
        .then(() => {
            console.log("Login: Success")
            navigation.navigate("Home")
        })
        .catch((err) => {
            console.log("Loginr: ", err)
            Alert.alert("Peringatan", "Login Anda Gagal, Silahkan Coba Lagi",)
        })
       
    }

    if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig)
    }
    return (
        <View style = {styles.Container}>
            <View style = {styles.Login}>
                <TextInput 
                    style = {styles.TextInput}
                    value = {email}
                    onChangeText = {(value) => setEmail(value)}
                    placeholder = "Masukan Email Anda"
                    keyboardType = "email-address"
                />

                <TextInput 
                    style = {styles.TextInput}
                    value = {passw}
                    onChangeText = {(value) => setPassw(value)}
                    secureTextEntry = {true}
                    placeholder = "Masukan Password Anda"
                />
                <View style = {styles.Submittion}> 
                    <TouchableOpacity style = {styles.Button} onPress ={() => navigation.navigate("Register")}>
                        <Text style = {styles.fontBtn}>I'm new!</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.Button} onPress = {() => Login()}>
                        <Text style ={styles.fontBtn}>Login</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#2565AE'
    },
    Submittion: {
        flexDirection: 'row',
        justifyContent: "space-between",
    },
    TextInput: {
        borderWidth: 1.3,
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderRadius: 6,
        marginBottom: 10,
    },
    Login: {
        width: '70%',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 30,
        justifyContent: 'center',
    },
    Button: {
        backgroundColor: '#2565AE', 
        alignItems: 'center', 
        borderRadius: 10,
        height: '50%',
        justifyContent: 'center',
        padding: 8

        
    },

    fontBtn: {
        color: 'white', 
        fontWeight: '600', 
        fontSize: 16,
    }
})
