import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

export default function AboutUs() {
    return (
        <View style = {styles.container}>
            <Text>Version: 0.0.1</Text>
            <Text>Dibuat oleh: Athallah Ariq Wijaya</Text>
            <Text>Tahap aplikasi: Pengembangan</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    }
})
