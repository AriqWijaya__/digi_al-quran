import React from 'react'
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native'
import * as firebase from 'firebase'
import { useSelector } from 'react-redux'
export default function Home({navigation}) {

    const signOut = () => {
        firebase.auth().signOut()
        .then(() => {
            console.log("SignOut: Berhasil")
            navigation.navigate("Login")            
        })
        .catch((err) => {
            console.log("SignOut: ", err)
        })
    }

    return (
        <View style = {styles.Container}>
            <View style = {styles.Home}>

                
                <TouchableOpacity style={styles.Button} onPress = {() => navigation.navigate("Surah")}>
                    <Text style ={styles.fontBtn}>Baca Qur'an</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.Button} onPress = {() => navigation.navigate("AboutUs")}>
                    <Text style ={styles.fontBtn}>Tentang Kami</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.Button} onPress = {() => signOut()}>
                    <Text style ={styles.fontBtn}>LogOut</Text>
                </TouchableOpacity>

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    Container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    Home: {
        width: '70%',
        justifyContent: 'space-evenly',
        height: '30%'
        
    },
    Button: {
        backgroundColor: '#2565AE', 
        alignItems: 'center', 
        paddingVertical: 10, 
        borderRadius: 60
    },

    fontBtn: {
        color: 'white', 
        fontWeight: '600', 
        fontSize: 16,
    }
})
