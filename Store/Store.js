import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '../CounterSlice/CounterSlice.js'

export const store = configureStore({
  reducer: {
      counter: counterReducer,
  },
})