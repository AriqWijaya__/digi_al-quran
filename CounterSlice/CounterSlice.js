import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'

const initialState = {
  idSurah: "",
  listSurah: [],
  firebaseConfig: {
    apiKey: "AIzaSyA0gytq3CTbslP5SvOSGPs94u-od6Eoskw",
    authDomain: "di-al-qur-an.firebaseapp.com",
    projectId: "di-al-qur-an",
    storageBucket: "di-al-qur-an.appspot.com",
    messagingSenderId: "6269249208",
    appId: "1:6269249208:web:7ddfbffdbcefd80fc8764d",
    measurementId: "G-LF92V54V5Y"
  },
  detailSurah: {nama: "", arti: ""},
}

export const counterSlice = createSlice({
    name: 'feature1',
    initialState,
    reducers: {
        SetSurah: (state, action) => {
            state.idSurah = action.payload.value
        },

        getSurah: (state) => {
            const getData = async () => {
                try {
                    const res = await axios.get('https://equran.id/api/surat')
                    const data1 = (res.data)
                    state.listSurah = data1
                    console.log(state.listSurah);
        
                } catch(error){
                    console.log(error)
                }
            }
            getData();
        },

        setNamaSurah: (state, action) => {
            
               state.detailSurah.nama = action.payload.name
                state.detailSurah.arti = action.payload.arti
            
        } 
    },
}) 

export const {SetSurah, getSurah, setNamaSurah} = counterSlice.actions

export default counterSlice.reducer